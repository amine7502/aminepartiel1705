<?php
//get the URI ( source without domain name)
$uri = $_SERVER['REQUEST_URI'];
 
if($result = match($uri, "/")){
    header("location:../articles");
    die;
}

####################
### Crud Article ###
####################

//Creation of createArticle's route
if($result = match($uri, "/article/create")){
    require("../Controller/article/createArticle.php");
    die;
}
//Creation of deleteArticle's route

if($result = match($uri, "/article/delete/:id")){
    require("../Controller/article/deleteArticle.php");
    die;
}
//Creation of displayArticle's route

if($result = match($uri, "/article/:id")){
    require("../Controller/article/displayArticle.php");
    die;
}

if($result = match($uri, "/articles")){
    require("../Controller/article/displayArticles.php");
    die;
}

if($result = match($uri, "/article/update/:id")){
    require("../Controller/article/updateArticle.php");
    die;
}

#################
### Crud User ###
#################
if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}

if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}

echo "No routes are matching with " . $uri;



function match($url, $route){
    $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);
    $regex = "#^$path$#i";
    if(!preg_match($regex, $url, $matches)){
        return false;
    }
    return true;
}