<?php

function getUsers(){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT u.id, u.email FROM user u WHERE 1') ;

    $response->execute(array());

    return $response;
}

function getUser($userId){
    $bdd = dbConnect(); 
    //connect to the database 
    // we create a variable with a prepared request to make a sql injection impossible

    $response = $bdd->prepare ('SELECT * FROM user 
    WHERE id = :userId') ;
// empty fiels because it's a prepared request
//the result is put on an array
    $response->execute(array('userId' => $userId));

    return $response;
}

//there is a redirection to the profile 
function createUser($bdd, $email, $password){

//Encryption turns character into a series of unreadable characters, decryption is reversable back, but hashing uses algorithms that generates variable characters, it's unverversable back
    $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
    $response = $bdd->prepare ('INSERT INTO `blog`.user(`email`, `password`)
            VALUES (:email, :password)');

    $response->execute(array('email' => $email, 'password' => $encryptedPassword));


    return $response;

}

function updateArticle($articleId, $author_id, $title, $body){
    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `article` SET 
                                `author_id`= :author_id,
                                `title`= :title ,
                                `body`= :body
                                WHERE id = :articleId') ;

    $response->execute(array(   'articleId' => $articleId,
                                'author_id' => $author_id,
                                'body' => $body,
                                'title' => $title
    ));

    return $response;
}



function deleteArticle($articleId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `article` WHERE id= :articleId') ;

    $response = $response->execute(array(   'articleId' => $articleId));

    return $response;
}



function dbConnect(){
    try
    {
        return new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

