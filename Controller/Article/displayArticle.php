<?php
$errors = new ArrayObject();
$title="display Article";
$articleId = getArticleIdFromURI();

require('../Model/articleRepository.php');

$response = getArticle($articleId);


if(!articleExist($response)){
    $errors->append('Sorry there is no article id ' . $articleId);
    displayErrors($errors);
    die;
}

ob_start();
displayErrors($errors);

require('../view/article/displayArticleView.php');

$content=ob_get_clean();

require('../view/templateView.php');

$response->closeCursor();


function getArticleIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}

function articleExist($response){
    if($response->rowCount() > 0 ){
        return true;
    }
    return false;

}