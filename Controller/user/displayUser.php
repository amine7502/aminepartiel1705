<?php

//we put errors in that PDO object witch will work like an array
$errors = new ArrayObject();
// Page title
$title="display User";
//we created that function to allows us to get directly the user's id from the URI
$userId = getUserIdFromURI();

//uploading the user repository
require('../Model/userRepository.php');

// Getting the user's id from the the result that brought us the ID from the uri
$response = getUser($userId);

//Verifying if the ID is correctly get
if(!userExist($response)){
    $errors->append('Sorry there is no user id ' . $userId);
    displayErrors($errors);
    die;
}

ob_start();
// Display all errors  in the array object past in parameter
displayErrors($errors);

//uploading the user's view
require('../view/user/displayUserView.php');
//reading the content of the buffer then it cleans it
// Lit le contenu courant du tampon de sortie puis l'efface
$content=ob_get_clean();

//Uploading the template
require('../view/templateView.php');

$response->closeCursor();


function getUserIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ; 
    $authorId = end($monUrl) ;

    return $authorId;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}

function userExist($response){
    if($response->rowCount() > 0 ){
        return true;
    }
    return false;

}
