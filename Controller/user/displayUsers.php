<?php

// The title of the page
$title="display Users";

// Upload of the model user 
require("../Model/userRepository.php");
//Get the users from the database
//Get the response that allows us to know if there is any user or users there with the pdostatement "getusers" ( of the original getattribute)
$response = getUsers();

if(usersExist($response)){
    // we upload the ( or import) the view
    require("../view/user/displayUsersView.php");
}else {
    echo 'Sorry there is no user wrote yet ';
}

//pdostatement 	that close the cursor and allows a new request
$response->closeCursor();


function usersExist($response){
    if($response->rowCount() > 0 ){
        return true;
    }
    return false;

}