<?php
//We create the user session using his infos
//thouse infos are stored in arrays
session_start();
$title="create User";
$errors = new ArrayObject();
//We store thouse infos in a simple form with three columns
if(isFormValid($errors)){

    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
//We import the repository by connecting in the database at first
    require("../Model/userRepository.php");
    $bdd = dbConnect();
    $response = createUser($bdd, $email, $password);

//we take the last user created 
    $lastInsertedId = $bdd->lastInsertId();
//The user profil creation result is under a boolean result
//We display that the error if the user creation cant be done
    if($lastInsertedId>0){
        header("location:../user/" . $lastInsertedId);die;
    }
    $errors->append("An error occurred, please contact your administrator system!");
}
//we start the output buffer ( temporisation de sortie)
ob_start();
displayErrors($errors);
//we upload the file that allows us to have the user's view
require("../view/user/createUserView.php");
//we clean the buffer
$content=ob_get_clean();
//we import the file of the template
require("../view/templateView.php");

//we create the fuction that allows us to see if the user is there for the first time
//if any info is submitted, the form can't sent
function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
//if fields are empty a message tells to the user to set the fields
    if(!fieldsArefilled($email, $password, $confirm_password)){
        $errors->append('All the fields must be filled');
        return false;
    }
//we verify if the the two passwords set by the user are simillar

    if($password != $confirm_password){
        $errors->append('confirmation password error');
        return false;
    }

    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['email']) AND isset($_POST['password']) AND isset($_POST['confirm_password'])){
        return true;
    }

    return false;
}

function fieldsArefilled($email, $password, $confirm_password){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($email) OR empty($password) OR empty($confirm_password)){
        return false;
    }
    return true;
}
//Function that displays other errors 
function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}




